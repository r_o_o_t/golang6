package main

import "fmt"

type fish interface {
	swim()
}

type reptiles interface {
	crawl()
}

type frog struct {
	sepcies string
}

func (frog) swim() {
	fmt.Println("It is swimming")
}

func (frog) crawl() {
	fmt.Println("It is crawling")
}

func main() {
	var fh fish
	var rp reptiles
	var fg = frog{"animal"}

	fh = fg
	fh.swim()

	rp = fg
	rp.crawl()
}
