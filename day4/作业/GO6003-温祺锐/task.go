package main

import (
	"errors"
	"fmt"
)


//1.
func reciprocal(args ...float64) (float64,error) {
	//判断不定长参数如果为0返回error
	if len(args) == 0 {
		return 0,errors.New("divde by zero")
	}
	var sum float64
	for i:=0;i<len(args);i++ {
		//判断长度到达索引边界，不再执行
		if i != len(args)-1{
			//值为0时返回error
			if args[i] == 0 || args[i+1] == 0 {
				return 0,errors.New("divde by zero")
			}
			sum = args[i] * args[i+1]
		}
	}
	return 1 / sum , nil
} 

//2.
func div(args ...float64) (float64, error) {
	//判断长度为0时返回error
	if len(args) == 0 {
		return 0, errors.New("divide by zero")
	}
	first := args[0]
	//第一个参数为0返回error
	if first == 0 {
		return 0, errors.New("divide by zero")
	}
	//当递归切片长度已为1时,return参数的倒数
	if len(args) == 1 {
		return 1 / first, nil
	}
	//依此递归取出从第二个参数到结尾的切片，直到切片长度为1
	remain := args[1:]
	res, err := div(remain...)
	if err != nil {
		return 0, err
	} else {
		return 1 / first * res, nil
	}
}

//3.
type Fish interface{
	Swimming(i int)
}

type Reptile interface{
	Crawl(i int)
}

type Frog struct {
	name string
}

func (frog Frog) Swimming(i int){
	fmt.Printf("%s游了%d圈\n",frog.name,i)
}
func (frog Frog) Crawl(i int){
	fmt.Printf("%s爬了%d圈\n",frog.name,i)
}

//4.
func square(num interface{}) interface{}{
	var res interface{}
	switch v := num.(type){
	case float32:
		fmt.Printf("num的类型是%T\n",v)
		res =  v * v
	case float64:
		fmt.Printf("num的类型是%T\n",v)
		res =  v * v
	case int:
		fmt.Printf("num的类型是%T\n",v)
		res =  v * v
	case byte:
		fmt.Printf("num的类型是%T\n",v)
		res =  v * v
	}
	return res 
}
func main () {
	res, err := div(1, 2, 3)
	fmt.Printf("%f %v\n", res, err)
	res1,err := reciprocal(1,2,3)
	fmt.Printf("%f %v\n",res1,err)
	fro := Frog{name: "青蛙王子"}
	fro.Swimming(2)
	fro.Crawl(2)
	var fish Fish
	var reptile Reptile
	fish = fro
	fish.Swimming(2)
	reptile = fro
	reptile.Crawl(2)
	res2 := square(2.0)
	fmt.Println(res2)
}