package main

/*
定义两个接口：鱼类和爬行动物，再定义一个结构体：青蛙，同时实现上述两个接口
 */

type Fisher interface {
	Swim()
}

type Crawler interface {
	Craw()
}

type Frog struct {}

func (Frog) Swim()  {}

func (Frog) Craw()  {}