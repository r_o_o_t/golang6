package main

import (
	"errors"
	"fmt"
)

/*
实现一个函数，接受若干个float64（用不定长参数）
返回这些参数乘积的倒数，除数为0时返回error
*/

func plusBottom(args ...float64) (float64, error) {
	var product float64 = 1.0
	for _, arg := range args {
		if arg == 0 {
			return product, errors.New("divide by zero")
		}
		product *= arg
	}
	return 1.0 / product, nil
}

func main() {
	args:=[]float64{1.3, 2.5, 3.7}
	//res, err := plusBottom(1.3, 2.5, 3.7)
	res, err := plusBottom(args...)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("result=%f\n", res)
	}
}