package main

import "fmt"

/*
实现函数func square(num interface{}) interface{}, 计算一个interface{}的平方
interface{}允许是4种类型: float32、float64、int、byte
*/

func square(num interface{}) interface{} {
	switch v := num.(type) {
	case int:
		//v := num.(int)
		return v * v
	case float32:
		return v * v
	case float64:
		return v * v
	case byte:
		return v * v
	default:
		fmt.Printf("unsupported data type %T\n", num)
		return nil
	}
}

func main() {
	var a int = 4
	var b float32 = 2
	var c float64 = 2
	var d byte = 2
	var e int32 = 2
	fmt.Println(square(a))
	fmt.Println(square(b))
	fmt.Println(square(c))
	fmt.Println(square(d))
	fmt.Println(square(e))
}