package main

import "fmt"

type fish interface {
	swim()
}
type animal interface {
	move()
}
type frog struct {
	name string
}

func (f frog) swim() { //frog实现fish接口
	fmt.Printf("%s会游泳。\n", f.name)
}
func (f frog) move() { //frog实现animal接口
	fmt.Printf("%s会爬行。\n", f.name)
}

func main() {
	var x fish
	var y animal
	f := frog{name: "青蛙王子"}
	x = f //接口可以存储实现该接口的实例
	x.swim()
	y = f
	y.move()
}
