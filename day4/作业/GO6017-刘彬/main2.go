package main

import (
	"errors"
	"fmt"
)

func product(n int, data ...float64) float64 { //递归实现求切片所有数字的乘积，n为切片长度
	if n == 1 {
		return data[0]
	}
	return data[n-1] * product(n-1, data[:n-1]...)
}

func reciprocal(data float64) (float64, error) { //定义求导数的函数
	if data == 0 {
		return -1, errors.New("不能对零求导数。") //errors.New返回格式化为给定文本的error
	}
	return 1 / data, nil
}

func main() {
	a := []float64{1, 2, 0}
	if v, err := reciprocal(product(len(a), a...)); err != nil { //a...表示切片a的扩展
		fmt.Println(err.Error())
	} else {
		fmt.Println(v)
	}
}
