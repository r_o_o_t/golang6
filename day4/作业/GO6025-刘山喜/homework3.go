package main

import (
	"fmt"

)

type Fish interface {
	swim(string) //游泳
}
type Reptile interface {
	crawl(string) //爬行
}
type frog struct{}

func (frog) swim(word string) {
	fmt.Println(word)
}
func (frog) crawl(word string) {
	fmt.Println(word)
}

func main() {
	Prince := frog{}
	fish := Prince
    reptile := Prince
	fish.swim("Gula~")
	reptile.crawl("Gua~Gua")
}
