package main

import (
	"errors"
	"fmt"
)

func reciprocal(args ...float64) (float64, error) { //计算倒数
	result := 1.0
	for _, v := range args {
		if v == 0 {
			return 0.0, errors.New("Invalid divisor: 0")
		}
		result *= v
	}
	return 1 / result, nil
}

func use(args ...float64) { //测试函数
	if value, err := reciprocal(args...); err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("result is %f\n", value)
	}
}
func main() {
	input1 := []float64{1.1, 2.2, 3.3}
	input2 := []float64{4.4, 5.5, 0}
	use(input1...) //测试正常的数据
	use(input2...) //测试error
}
