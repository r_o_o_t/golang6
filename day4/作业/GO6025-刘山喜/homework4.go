package main

import "fmt"

func square(num interface{}) interface{} {
	switch v := num.(type) {
	case float64:
		return v * v
	case float32:
		return v * v
	case int:
		return v * v
	case byte:
		return v * v
	default:
		return "Invalid number!"
	}
}
func main() {
	fmt.Println(square(3.14))          //测试合法类型
	fmt.Println(square("hello,world")) //测试非法类型
}
