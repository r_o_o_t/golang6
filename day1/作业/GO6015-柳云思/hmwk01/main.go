package main

import (
	"fmt"
	"hmwk01/myfuncs"
)

func main() {

	fmt.Printf("Binary    0 is: %s\n", myfuncs.BinaryFormat(0))
	fmt.Printf("Binary    1 is: %s\n", myfuncs.BinaryFormat(1))
	fmt.Printf("Binary   -1 is: %s\n", myfuncs.BinaryFormat(-1))
	fmt.Printf("Binary  260 is: %s\n", myfuncs.BinaryFormat(260))
	fmt.Printf("Binary -260 is: %s\n", myfuncs.BinaryFormat(-260))

}
