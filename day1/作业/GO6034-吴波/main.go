package main

import (
	"fmt"
	"math"
	"strings"
)


func BinaryFormat(n int32) string {
	a := uint32(n)
	sb := strings.Builder{}	  // 字符串连接，将两个字符串拼接成一个字符串
	c := uint32(math.Pow(2, 31))    // 最高位是1，其余位是0
	for i := 0; i < 32; i++ {
		if a&c != 0 {  // 判断n的当前位上是否是1
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		c >>= 1。  // 1往右移一位
	}
	return sb.String()
}


func main()  {
	fmt.Printf("os arch %s\n", BinaryFormat(160))
	fmt.Printf("os arch %s\n", BinaryFormat(-160))
}
//方法和结果都是对的，可以尝试一下使用另外的位运算方式来实现
