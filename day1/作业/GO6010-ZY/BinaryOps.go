package main
import(
	"fmt"
	"strings"
	"math"
)

func BinaryFormat(n int32) string{
    a := uint32(n)
    sb := strings.Builder{}
    c := uint32(math.Pow(2, 31))
    for i := 0; i < 32; i++ {
        if  a & c > 0 { 
            sb.WriteString("1")
        } else {
            sb.WriteString("0")
        }
        c = c >> 1 
    }
    return sb.String()	
}

func ShowBinary(){
	fmt.Printf("0 的二进制表示为%d\n",BinaryFormat(0))
	fmt.Printf("1 的二进制表示为%d\n",BinaryFormat(1))
	fmt.Printf("-1 的二进制表示为%d\n",BinaryFormat(-1))
	fmt.Printf("260 的二进制表示为%d\n",BinaryFormat(260))
	fmt.Printf("-260 的二进制表示为%d\n",BinaryFormat(-260))
}

func main(){
	ShowBinary()
}
	
//方法和结果都是对的，可以尝试一下使用另外的位运算方式来实现