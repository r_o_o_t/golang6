package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	fmt.Println("0 BinaryFormat is ", BinaryFormat(0)+"\n")
	fmt.Println("1 BinaryFormat is ", BinaryFormat(1)+"\n")
	fmt.Println("-1 BinaryFormat is ", BinaryFormat(-1)+"\n")
	fmt.Println("260 BinaryFormat is ", BinaryFormat(260)+"\n")
	fmt.Println("-260 BinaryFormat is ", BinaryFormat(-260)+"\n")
}

func BinaryFormat(n int32) string {
	a := uint32(n) //
	sb := strings.Builder{}
	c := uint32(math.Pow(2, 31))
	for i := 0; i < 32; i++ {
		if a&c != 0 {
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		c >>= 1
	}
	return sb.String()
}

//方法和结果都是对的，可以尝试一下使用另外的位运算方式来实现