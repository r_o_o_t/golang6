package main

import (
	"fmt"
	bf "homework/binaryFormat"
)

// 0、1、-1、260、-260

func main() {
	fmt.Println(bf.BinaryFormat(0))
	fmt.Println(bf.BinaryFormat(1))
	fmt.Println(bf.BinaryFormat(-1))
	fmt.Println(bf.BinaryFormat(260))
	fmt.Println(bf.BinaryFormat(-260))
}

//方法和结果都是对的，可以尝试一下使用另外的位运算方式来实现