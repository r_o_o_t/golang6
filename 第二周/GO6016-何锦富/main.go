package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

//1.创建一个初始长度为0、容量为10的int型切片，调用rand.Intn(128)100次，往切片里面添加100个元素，利用map统计该切片里有多少个互不相同的元素。
func map_counter() map[int]int {
	slice1 := make([]int, 0, 10)
	//明确知道大小可以避免内存多次申请，因为map需要hash计算消耗资源
	map1 := make(map[int]int, 100)
	//100次取随机数
	for i := 0; i < 100; i++ {
		rand := rand.Intn(128)
		slice1 = append(slice1, rand)
		map1[rand] += 1
	}
	return map1
}

//2.实现一个函数func arr2string(arr []int) string，比如输入[]int{2,4,6}，返回“2 4 6”。输入的切片可能很短，也可能很长。
func arr2string(arr []int) string {
	if len(arr) == 0 {
		return ""
	}
	sb := strings.Builder{}
	for i := 0; i < len(arr); i++ {
		st := strconv.Itoa(arr[i])
	
		sb.WriteString(st+" ")
	}
	return sb.String()
}

//
func main() {
	//第一题
	fmt.Println(arr2string([]int{1,2,3,100}))

	//第二题
	fmt.Println(map_counter())

}
