package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

func statistics() {
	rand.Seed(time.Now().Unix())
	var number []int
	var dict = map[int]int{}
	var total = 0
	for i := 0; i < 100; i++ {
		number = append(number, rand.Intn(128))
	}
	for _, v := range number {
		if _, ok := dict[v]; ok {
			continue
		}
		dict[v] = v
		total += 1
	}
	fmt.Printf("不同元素为%d个\n", total)
}

func join(arr []int) string {
	if len(arr) == 0 {
		return ""
	}
	str := strings.Builder{}
	for _, v := range arr {
		str.WriteString(fmt.Sprintf("%d", v))
		str.WriteString(" ")
	}
	return str.String()
}

func main() {
	// 统计不同元素
	statistics()

	// 字符串拼接
	fmt.Println("字符串拼接:", join([]int{1, 2, 3}))
}
