package main

import (
	"fmt"
	"math/rand"
	"time"
)

/*
创建一个初始长度为0、容量为10的int型切片，调用rand.Intn(128)100次，
往切片里面添加100个元素，利用map统计该切片里有多少个互不相同的元素
*/

func main() {
	countN()
}

func countN() {
	intSlice := make([]int, 0, 10)
	m1 := make(map[int]int, 100)
	rand.Seed(time.Now().Unix())
	for i := 0; i < 100; i++ {
		intSlice = append(intSlice, rand.Intn(128))
	}
	// fmt.Println(intSlice)
	// fmt.Println(len(intSlice))
	for _, v := range intSlice {
		if _, is_exists := m1[v]; is_exists {
			m1[v]++
		} else {
			m1[v] = 1
		}
	}
	// fmt.Println(m1)
	for idx, val := range m1 {

		fmt.Printf("在m1 中 %d 出现%d\n", idx, val)
	}

}
