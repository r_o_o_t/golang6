package main

import (
	"fmt"
	"strconv"
	"strings"
)

/*
实现一个函数func arr2string(arr []int) string，比如输入[]int{2,4,6}，返回“2 4 6”。
输入的切片可能很短，也可能很长
*/

func main() {
	arr1 := []int{1, 2, 3, 4, 6, 7, 9}
	fmt.Printf("v is %s, type is %T\n", arr2string(arr1), arr2string(arr1))
	fmt.Printf("v is %s, type is %T\n", arr2string2(arr1), arr2string2(arr1))
	fmt.Printf("v is %s, type is %T\n", arr2string3(arr1), arr2string3(arr1))
	fmt.Printf("v is %s, type is %T\n", arr2string4(arr1), arr2string4(arr1))
}

func arr2string(arr []int) (ret string) {
	if len(arr) == 0 {
		return
	}
	for _, v := range arr {
		ret += strconv.Itoa(v) + " "
	}
	return
}

func arr2string2(arr []int) (ret string) {
	if len(arr) == 0 {
		return
	}
	for _, v := range arr {
		ret += fmt.Sprintf(strconv.Itoa(v) + " ")
	}
	return
}

func arr2string3(arr []int) (ret string) {
	if len(arr) == 0 {
		return
	}
	ssli := []string{}
	for _, v := range arr {
		ssli = append(ssli, strconv.Itoa(v))
	}
	ret = strings.Join(ssli, " ")
	return
}

func arr2string4(arr []int) (ret string) {
	if len(arr) == 0 {
		return
	}
	s1 := strings.Builder{}
	for _, v := range arr {
		s1.WriteString(strconv.Itoa(v))
		s1.WriteString(" ")
	}
	ret = s1.String()
	return
}
