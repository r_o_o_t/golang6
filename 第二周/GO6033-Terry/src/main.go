package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

func diffrentEle() {
	slice := make([]int, 0, 10)
	uniqueKeys := make(map[int]int)

	for i := 0; i < 100; i++ {
		rand.Seed(time.Now().UnixNano())
		num := rand.Intn(128)
		slice = append(slice, num)
		if _, ok := uniqueKeys[num]; ok == false {
			uniqueKeys[num] = i
			fmt.Printf("数字新加 %d\n", num)
		}
	}

	fmt.Printf("总共有%d个不同的数字\n", len(uniqueKeys))
}

func arr2String(arr []int) string {
	var builder strings.Builder
	for _, num := range arr {
		fmt.Fprintf(&builder, "%d", num)
	}
	s := builder.String()
	return s
}

func main() {
	fmt.Println("------------第一个作业------------")
	diffrentEle()

	fmt.Println("------------第二个作业------------")

	var datas []int
	datas = []int{2, 3, 5, 6, 7, 8, 91, 10, 100, 2010}
	dataStr := arr2String(datas)
	fmt.Printf("得到的string为：%s", dataStr)
}
