package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

// 1. 元素统计
func mapStatistic() {

	randSlice := make([]int, 0, 10)
	mapSts := make(map[int]bool)

	//Create a random slice
	for i := 0; i < 100; i++ {
		n := rand.Intn(128)
		randSlice = append(randSlice, n)
	}

	// the statistic for map
	for _, v := range randSlice {
		if _, ok := mapSts[v]; !ok {
			mapSts[v] = true
		}
	}
	fmt.Printf("randSlice: %v\n", randSlice)
	fmt.Printf("mapSts: %v\n", mapSts)
	fmt.Printf("The length of mapSts is: %d\n", len(mapSts))
}

// 2. arr2string
func arr2String(arr []int) string {

	sbd := strings.Builder{}
	for _, v := range arr {
		sbd.WriteString(strconv.Itoa(v))
	}

	// Insert whitespace between all letters including heading and trailing
	sbdStr := sbd.String()
	separatedSbdStr := strings.Replace(sbdStr, "", " ", -1)

	return strings.TrimSpace(separatedSbdStr)
}

func main() {
	// 1.
	mapStatistic()

	fmt.Println("----------------------------------------")
	// 2.
	arr := []int{1, 2, 5, 8}
	fmt.Printf("[]int -> string: %s\n", arr2String(arr))

}

//实现的非常不错，第一题里，函数也直接返回结果，不要打印
