package main

import (
	"fmt"
	"train/hw"
)

func main() {
	a1r1 := hw.GetNumOfDiffEle()
	fmt.Printf("1.创建一个初始长度为0、容量为10的int型切片，调用rand.Intn(128)100次，往切片里面添加100个元素，利用map统计该切片里有多少个互不相同的元素。\n Answer: %+v \n", a1r1)

	a2s1 := []int{1231231, 123123123, 12312312312312312, 12313123, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	a2r1 := hw.Arr2string(a2s1)
	a2s2 := []int{}
	a2r2 := hw.Arr2string(a2s2)
	fmt.Printf(`2.实现一个函数func arr2string(arr []int) string，比如输入[]int{2,4,6}，返回“2 4 6”。输入的切片可能很短，也可能很长。
		[]int: %v  ---> string: '%v' 
		[]int: %v  ---> string: '%v' `,
		a2s1, a2r1,
		a2s2, a2r2)

	fmt.Println("")
}
