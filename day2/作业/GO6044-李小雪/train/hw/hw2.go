package hw

import (
	"strconv"
	"strings"
)

// Arr2string homework for 20210925.
// 将任意长度的数值类型切片拼接成字符串.
func Arr2string(arr []int) string {
	var rs strings.Builder
	for _, v := range arr {
		rs.WriteString(strconv.Itoa(v))
	}
	return rs.String()
}
