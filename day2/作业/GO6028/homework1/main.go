package main

import (
	"fmt"
	"math/rand"
)

/*
创建一个初始长度为0、容量为10的int型切片
调用rand.Intn(128)100次，往切片里面添加100个元素
利用map统计该切片里有多少个互不相同的元素
 */

func main()  {
	sas := make([]int, 0, 10)
	for i:=0;i<100;i++{
		sas = append(sas, rand.Intn(128))
	}
	m := make(map[int]bool)
	for _, v := range sas {
		if _, ok := m[v]; !ok {
			m[v] = true
		}
	}
	//fmt.Printf("切片中元素为 %v\n", sas)
	//fmt.Printf("map为 %v\n", m)
	fmt.Printf("map中互不相同的元素个数为 %d\n", len(m))
}



