package main

import (
	"fmt"
	"strconv"
	"strings"
)

/*
实现一个函数func arr2string(arr []int) string，
比如输入[]int{2,4,6}，返回“2 4 6”
输入的切片可能很短，也可能很长
 */

func arr2string(arr []int) string {
	stb := strings.Builder{}
	for _, v := range arr {
		stb.WriteString(strconv.Itoa(v))  // int转str 字符串
	}
	//fmt.Println(stb)
	stb2str := strings.Replace(stb.String(), "", " ", -1)  // 246 ——> 2 4 6
	//fmt.Printf("%T\n", stb2str)  // string
	return strings.TrimSpace(stb2str)
}

func main() {
	arr := []int{2, 4, 6, 8}
	fmt.Printf("arr is %d\n", arr)  // arr is [2 4 6 8]
	fmt.Printf("arr2string result is %#v\n", arr2string(arr))  // arr2string result is "2 4 6 8"
}


