package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

/*
1.创建一个初始长度为0、容量为10的int型切片，调用rand.Intn(128)100次，往切片里面添加100个元素，利用map统计该切片里有多少个互不相同的元素。

2.实现一个函数func arr2string(arr []int) string，比如输入[]int{2,4,6}，返回“2 4 6”。输入的切片可能很短，也可能很长。

*/

func main() {
	question1()

	fmt.Println(arr2string([]int{2, 4, 6}))

}

// 习题1
func question1() {

	slice1 := make([]int, 10)
	for i := 0; i < 100; i++ {
		randNum := rand.Intn(128)
		slice1 = append(slice1, randNum)
	}
	fmt.Println(slice1)

	map1 := make(map[int]int)
	for _, v := range slice1 {
		// 以随机数为key,每次遇到同样的key就把value + 1, 用来计数
		map1[v] = map1[v] + 1
	}

	for k, v := range map1 {
		fmt.Printf("slice1的切片中有%d个%d\n", v, k)
	}

}

// 习题2
func arr2string(arr []int) string {
	slice2 := []string{}
	for _, v := range arr {
		slice2 = append(slice2, strconv.Itoa(v))
	}

	return strings.Join(slice2, " ")

}

// 方法1使用return返回结果，不要使用printf打印结果
