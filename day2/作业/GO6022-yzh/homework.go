package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

/*
1.创建一个初始长度为0、容量为10的int型切片，调用rand.Intn(128)100次，往切片里面添加100个元素，利用map统计该切片里有多少个互不相同的元素。

2.实现一个函数func arr2string(arr []int) string，比如输入[]int{2,4,6}，返回“2 4 6”。输入的切片可能很短，也可能很长。
*/
func question1() int {
	arr := make([]int, 0, 10)

	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 100; i++ {
		arr = append(arr, rand.Intn(128))
	}

	m1 := make(map[int]int, 100)
	for _, v := range arr {
		m1[v] += 1
	}

	return len(m1)
}

func arr2string(arr []int) string {
	s1 := strings.Builder{}
	for _, v := range arr {
		s1.WriteString(strconv.FormatInt(int64(v), 10) + " ")
	}

	return strings.TrimSpace(s1.String())
}

func main() {
	fmt.Println(question1())
	var a []int
	b := make([]int, 0)
	c := make([]int, 100)
	fmt.Printf("%#v\n", arr2string(a))
	fmt.Printf("%#v\n", arr2string(b))
	fmt.Printf("%#v\n", arr2string(c))
}

//方法1使用return返回结果，不要使用printf打印结果
