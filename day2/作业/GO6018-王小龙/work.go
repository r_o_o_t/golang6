package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strconv"
)


func part01() {
	arr := make([]int,0,10)
	dir := make(map[int]int)

	for i := 0; i < 100; i++ {
		num := rand.Intn(128)
		arr = append(arr, num)
		dir[num] += 1
	}
	fmt.Printf("Create an array of 100 lengths\n%v\n",arr)
	total_num := 0
	for _,v := range dir {
		if v != 1 { //等于1的情况为什么要排除
			total_num += 1
		}
	}
	fmt.Printf("There are %d elements that are different from each other\n",total_num)
}


func arr2string(arr []int)  string {
	var spli_string bytes.Buffer
	for i,v := range arr {
		
		if i == len(arr)-1 {
			spli_string.WriteString(strconv.Itoa(v))
		}else{
			spli_string.WriteString(strconv.Itoa(v)+" ")
		}
	}
	str := spli_string.String()
	return str
}

func main() {
	fmt.Println("--------------------------------查找互不相同元素--------------------------------")
	part01()

	// 切片转字符串
	fmt.Println("--------------------------------切片转字符串--------------------------------")
	var arr = []int{2,4,6}
	res := arr2string(arr)
	fmt.Printf("%v 切片转换为字符串 %s\n",arr,res)
}

// 结果是对的，在生产上，函数使用返回值返回结果，不要使用打印