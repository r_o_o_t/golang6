package main

import (
	"fmt"
	"math/rand"
)

func main() {
	rands := make([]int, 0, 10)
	counts := make(map[int]int)
	//generate random numbers and statistics counts
	for i := 0; i < 100; i++ {
		rands = append(rands, rand.Intn(128))
		counts[rands[i]]++
	}
	// output result
	fmt.Printf("value\tcount\n")
	for i := 0; i < 100; i++ {
		rand := rands[i]
		fmt.Printf("%-5v\t%-v\n", rand, counts[rand])
	}
}
// 统计结果的方式太复杂，可以更简单一点，例如输出字典长度
