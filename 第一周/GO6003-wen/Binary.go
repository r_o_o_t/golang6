package main

import (
	"fmt"
	"math"
	"strings"
)

//输出int32对应的二进制
func Binary(n int32) string {
	a := uint32(n)
	sb := strings.Builder{}
	c := uint32(math.Pow(2, 31)) //最高位上是1，其他位为0
	for i := 0; i < 32; i++ {
		if a&c != 0 { //判断当前位是否为1
			sb.WriteString("1")
		} else { //不为1则为0
			sb.WriteString("0")
		}
		c >>= 1 //右边移一位
	}
	return sb.String()
}

func main() {
	fmt.Println(Binary(260))
	fmt.Println(Binary(1))
}
