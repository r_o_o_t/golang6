package main

import "fmt"

func monthToSeason(m int) {
	if m >= 10 && m <= 12 {
		fmt.Println("Winter")
	} else if m >= 7 && m <= 9 {
		fmt.Println("Autumn")
	} else if m >= 4 && m <= 6 {
		fmt.Println("Summer")
	} else if m >= 1 && m <= 3 {
		fmt.Println("Spring")
	} else {
		fmt.Println("Invalid value")
	}
}

func monthToSeason2(m int) {
	switch m {
	case 1, 2, 3:
		fmt.Println("Spring")
	case 4, 5, 6:
		fmt.Println("Summer")
	case 7, 8, 9:
		fmt.Println("Autumn")
	case 10, 11, 12:
		fmt.Println("Winter")
	default:
		fmt.Println("Invalid value")
	}
}

func main() {
	var m int
	fmt.Print("请输入要查询的月份(1-12): ")
	fmt.Scan(&m)
	monthToSeason(m)
	monthToSeason2(m)
}

// 函数的结果尽量用return返回，调试的时候可以使用fmt.Println，生产上尽量不要使用。
