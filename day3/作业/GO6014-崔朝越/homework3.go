/*
3.创建一个student结构体，包含姓名和语数外三门课的成绩。用一个slice容纳一个班的同学，求每位同学的平均分和整个班三门课的平均分，全班同学平均分低于60的有几位
*/

package main

import (
	"fmt"
	"math/rand"
)

type Student struct {
	Name    string
	Chinese float32
	Math    float32
	English float32
}

type ClassAverageScore struct {
	Caverage float32
	Maverage float32
	Eaverage float32
}

//生成一个50位同学的班级
func generateClass() (stus []*Student) {
	for i := 1; i <= 50; i++ {
		stu := &Student{
			Name:    fmt.Sprintf("stu%02d", i),
			Chinese: rand.Float32() * 100,
			Math:    rand.Float32() * 100,
			English: rand.Float32() * 100,
		}
		stus = append(stus, stu)
	}
	return
}

//求一位同学的平均分
func (s *Student) averageScore() float32 {
	sumScore := s.Chinese + s.Math + s.English
	averageScore := sumScore / 3
	return averageScore
}

//求全班同学每个科目的平均分
func (ca *ClassAverageScore) classAverageScore(stus []*Student) *ClassAverageScore {
	sumStu := len(stus)
	var sumC, sumM, sumE float32
	for _, stu := range stus {
		sumC += stu.Chinese
		sumM += stu.Math
		sumE += stu.English
	}
	ca.Caverage = sumC / float32(sumStu)
	ca.Maverage = sumM / float32(sumStu)
	ca.Eaverage = sumE / float32(sumStu)
	return ca
}

func main() {
	stus := generateClass()
	scoreMap := make(map[string]float32, len(stus))
	lower60 := 0

	for _, stu := range stus {
		averageScore := stu.averageScore()
		scoreMap[stu.Name] = averageScore
		if averageScore < 60 {
			lower60++
		}
	}
	fmt.Println("每个人的平均分:", scoreMap)
	fmt.Println("小于60分的人数:", lower60)

	var caAverage = new(ClassAverageScore)
	caAverage = caAverage.classAverageScore(stus)
	fmt.Println("全班三门课的平均分:", *caAverage)

}
