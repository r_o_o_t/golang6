package main

import "fmt"

func season1(mon int) {
	if mon < 1 || mon > 12 {
		fmt.Println("月份输入有误！")
	} else if mon == 3 || mon == 4 || mon == 5 {
		fmt.Println("spring")
	} else if mon == 6 || mon == 7 || mon == 8 {
		fmt.Println("summer")
	} else if mon == 9 || mon == 10 || mon == 11 {
		fmt.Println("autumn")
	} else {
		fmt.Println("winter")
	}
}

func season2(mon int) {
	switch mon {
	case 3, 4, 5:
		fmt.Println("spring")
	case 6, 7, 8:
		fmt.Println("summer")
	case 9, 10, 11:
		fmt.Println("autumn")
	case 12, 1, 2:
		fmt.Println("winter")
	default:
		fmt.Println("月份输入有误！")
	}
}

func main() {
	season1(12)
	season2(5)
}
