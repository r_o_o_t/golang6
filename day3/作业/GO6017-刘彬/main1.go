package main

import (
	"fmt"
	"math/rand"
	"time"
)

func genMatrix(m, n int) [][]int {
	mat := make([][]int, 0)
	for i := 0; i < m; i++ {
		line := make([]int, n)
		mat = append(mat, line)
	}
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			mat[i][j] = rand.Intn(100)
		}
	}
	return mat
}

func addMatrix(x, y [][]int) [][]int {
	z := make([][]int, len(x))
	for i := range z {
		z[i] = make([]int, len(x[0]))
	}
	for i := 0; i < len(x); i++ {
		for j := 0; j < len(x[0]); j++ {
			z[i][j] = x[i][j] + y[i][j]
		}
	}
	return z
}

func main() {
	rand.Seed(time.Now().Unix())
	m1 := genMatrix(8, 5)
	fmt.Println(m1)
	m2 := genMatrix(8, 5)
	fmt.Println(m2)
	m := addMatrix(m1, m2)
	fmt.Println(m)
}
