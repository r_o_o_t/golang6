package main

import "fmt"

// 创建一个student结构体，包含姓名和语数外三门课的成绩。
// 用一个slice容纳一个班的同学，求每位同学的平均分和整个班三门课的平均分，全班同学平均分低于60的有几位

type score struct {
	chinese float64
	math    float64
	english float64
	avg     float64
}

type student struct {
	name string
	*score
}

type class struct {
	name     string
	students []*student
	score    float64
}

func (s *score) GetAvgScore() {
	s.avg = (s.chinese + s.math + s.english) / 3
}

func main() {
	s1 := student{name: "张三", score: &score{chinese: 91, math: 86, english: 77}}
	s2 := student{name: "李四", score: &score{chinese: 0, math: 0, english: 0}}
	s3 := student{name: "王五", score: &score{chinese: 34, math: 70, english: 51}}

	s1.GetAvgScore()
	fmt.Printf("%s 语文成绩: %g 数学成绩: %g 外语成绩为: %g 平均成绩为: %.2f\n", s1.name, s1.chinese, s1.math, s1.english, s1.avg)

	s2.GetAvgScore()
	fmt.Printf("%s 语文成绩: %g 数学成绩: %g 外语成绩为: %g 平均成绩为: %.2f\n", s2.name, s2.chinese, s2.math, s2.english, s2.avg)

	s3.GetAvgScore()
	fmt.Printf("%s 语文成绩: %g 数学成绩: %g 外语成绩为: %g 平均成绩为: %.2f\n", s3.name, s3.chinese, s3.math, s3.english, s3.avg)

	class := class{name: "GO第6期", students: []*student{&s1, &s2, &s3}}
	var sum float64
	var count int
	for _, v := range class.students {
		if v.score.avg < 60 {
			count += 1
		}
		sum += v.score.avg
	}
	fmt.Printf("%s 班级总体平均成绩 %g\n", class.name, sum/float64(len(class.students)))
	fmt.Printf("%s 班级平均分小于60的有 %d\n", class.name, count)

}
