package main

import "fmt"

type student struct {
	name                   string
	chinese, math, english int16
}

type class struct {
	students []student
}

func (s student) getPersonalAvgScore() (name string, personalAvgScore float32) {
	personalAvgScore = float32(s.chinese+s.math+s.english) / 3
	name = s.name
	return
}

func (s student) isPassStudent() (string, bool) {
	passline := float32(60)
	name, avgScore := s.getPersonalAvgScore()
	if avgScore < passline {
		return name, false
	} else {
		return name, true
	}

}

func (class class) getAllPersonAvgScore() (chineseAvgScore, mathAvgScore, englishScore float32) {
	c := []int16{}
	m := []int16{}
	e := []int16{}

	// 下面的变量命名尽量表示它本身的含义, 不要使用这种单个字母
	for _, s := range class.students {
		c = append(c, s.chinese)
		m = append(m, s.math)
		e = append(e, s.english)
	}
	chineseAvgScore = avg(c)
	mathAvgScore = avg(m)
	englishScore = avg(e)
	return
}

func (class class) getNumOfNoPass(passline float32) (noPassNum int) {
	noPassNum = 0
	for _, s := range class.students {
		if _, score := s.getPersonalAvgScore(); score < passline {
			noPassNum++
		}
	}

	return
}

func avg(s []int16) (r float32) {
	var sum int16
	for _, v := range s {
		sum += v
	}
	r = float32(sum) / float32(len(s))
	return
}

func main() {
	//1.随机初始化两个8*5的矩阵，求两个矩阵的和（逐元素相加）

	//2.给定月份，判断属于哪个季节。分别用if和switch实现

	//3.创建一个student结构体，包含姓名和语数外三门课的成绩。用一个slice容纳一个班的同学，求每位同学的平均分和整个班三门课的平均分，全班同学平均分低于60的有几位
	var students = []student{
		{"alice", 50, 50, 70},
		{"bruce", 30, 90, 70},
		{"lucy", 53, 80, 70},
		{"joy", 78, 63, 76},
	}

	var class class = class{students} //这里不要将变量名和类型名使用相同的名称，避免误解

	// fmt.Println(students)
	// fmt.Println(students[0].getPersonalAvgScore())
	// fmt.Println(getAllPersonAvgScore(students))

	for _, s := range students {
		name, personalAvg := s.getPersonalAvgScore()
		fmt.Printf("Student %s avg score: %3f \n", name, personalAvg)
	}

	fmt.Println("")

	for _, s := range students {
		name, isPass := s.isPassStudent()
		if !isPass {
			fmt.Printf("Student %s is not pass. \n", name)
		}
	}

	fmt.Println("")

	chineseAvg, mathAvg, englishAvg := class.getAllPersonAvgScore()
	fmt.Printf(`The Class Avg score : 
	Chinese: %3f 
	Math:	 %3f 
	English: %3f 
	`, chineseAvg, mathAvg, englishAvg)
	fmt.Println("")

	fmt.Printf("The num about students of NoPass is %d \n", class.getNumOfNoPass(float32(60)))
}

// 变量命名要尽量规范，函数返回值必须要给出，上面才能打印出正确结果