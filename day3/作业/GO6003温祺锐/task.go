package main

import (
	"fmt"
	"math/rand"
	_"strconv"
	"time"
)

func nest() {
	//初始化矩阵的大小
	const SIZE1 = 8
	const SIZE2 = 4

	//初始化随机数种子
	rand.Seed(time.Now().Unix())

	//初始化两个矩阵
	A := [SIZE1][SIZE2]int{}
	B := [SIZE1][SIZE2]int{}
	
	//为两个矩阵添加元素,并且求和
	sum :=0
	for i:=0;i<SIZE1;i++{
		for j:=0;j<SIZE2;j++{
			A[i][j] = rand.Intn(10)
			B[i][j] = rand.Intn(10)
			sum += (A[i][j] + B[i][j])
		}
	}
	fmt.Printf("两个矩阵的和为：%d",sum)
}

// 矩阵相加的和也是一个矩阵，不是把矩阵内所有元素加起来的和

func Judging_season_if(){
	//获取当前月份
	now := time.Now()
	fmt.Println(now.Month())
	if now.Month().String() == "April" || now.Month().String() == "May" ||  now.Month().String() == "June" {
		fmt.Println("春天")
	} else if now.Month().String() == "July" || now.Month().String() == "August" ||  now.Month().String() == "September" {
		fmt.Println("夏天")
	} else if now.Month().String() == "October" || now.Month().String() == "November" ||  now.Month().String() == "December" {
		fmt.Println("秋天")
	} else{
		fmt.Println("冬天")
	}
	
}

func  Judging_season_switch(){
	now := time.Now()
	fmt.Println(now.Month())
	switch now.Month().String(){
	case "April","May","June":
		fmt.Println("春天")
	case "July","August","September":
		fmt.Println("夏天")
	case "October","November","December":
		fmt.Println("秋天")
	default:
		fmt.Println("冬天")
	}
}

func student(){
	//定义成绩结构体
	type achievement struct {
		chinese int 
		mathematics int
		english int
	}
	//定义学生结构体
	type student struct {
		name string
		achie achievement
	}
	var class []student

	//初始化随机数种子
	rand.Seed(time.Now().Unix())	
	for i:=0;i<30;i++{
		//添加班级成员和成绩
		stu :=new(student)
		stu.name = string(rune(rand.Int63n(40869-19968)))
		stu.achie.chinese = rand.Intn(100)
		stu.achie.mathematics = rand.Intn(100)
		stu.achie.english = rand.Intn(100)
		class = append(class, *stu)
	}

	var class_ave int
	var loser int
	for i:=0;i<len(class);i++{
		//计算每个成员的平均分
		average := (class[i].achie.chinese + class[i].achie.mathematics + class[i].achie.english ) / 3
		//统计全班平均分低于60分的有多少人
		if average < 60 {
			loser ++
		}
		//累加全班的总平均分
		class_ave += average
		fmt.Printf("姓名:%s\n 语文:%d\n 数学:%d\n 英语:%d\n 平均分:%d\n\n\n",class[i].name,class[i].achie.chinese,class[i].achie.mathematics,class[i].achie.english,average)
	}
	//计算全班平均分
	fmt.Printf("全班平均分:%d,低于60分的有%d人",class_ave / 30,loser)
	
}

func main() {
	// nest()
	// Judging_season_if()
	// Judging_season_switch()
	student()
}
