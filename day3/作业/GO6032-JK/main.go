package main

import (
	"fmt"
	"math/rand"
)

type Student struct {
	Name                 string
	Yuwen, Math, English float64
}

func main() {
	job1(7)
	job2(7)
	job3(7)

}

func job1(maxNumber int) {
	a1 := [8][5]int{}
	b1 := a1
	sum := b1

	for i := 0; i < 8; i++ {
		for j := 0; j < 5; j++ {
			a1[i][j] = rand.Intn(maxNumber)
			b1[i][j] = rand.Intn(maxNumber)
			sum[i][j] = a1[i][j] + b1[i][j]
		}
	}
	fmt.Printf("矩阵 a1 的值为: %d\n", a1)
	fmt.Printf("矩阵 b1 的值为: %d\n", b1)
	fmt.Printf("两个矩阵和的值为: %d", sum)

}

func job2(months int) {
	/*
		2-4     春
		5-7     夏
		8-10    秋
		11-12-1 冬

	*/

	if months < 1 || months > 12 {
		fmt.Printf("月份输出错误，请输入 1-12 数字\n")
	} else if months >= 2 && months <= 4 {
		fmt.Printf("%d 月属于春季\n", months)
	} else if months >= 5 && months <= 7 {
		fmt.Printf("%d 月属于夏季\n", months)
	} else if months >= 8 && months <= 10 {
		fmt.Printf("%d 月属于秋季\n", months)
	} else if months >= 11 && months <= 12 || months == 1 {
		fmt.Printf("%d 月属于冬季\n", months)
	}

	switch months {
	case 2, 3, 4:
		fmt.Printf("%d 月属于春季", months)
	case 5, 6, 7:
		fmt.Printf("%d 月属于夏季", months)
	case 8, 9, 10:
		fmt.Printf("%d 月属于秋季", months)
	case 11, 12, 1:
		fmt.Printf("%d 月属于冬季", months)
	default:
		fmt.Printf("月份输出错误，请输入 1-12 数字")

	}
}

func job3(StudentNumber int) {
	var SumStudent float64 // 所有学生总分数
	s1 := []string{}       //平均分低于 60 的学生
	for i := 0; i < StudentNumber; i++ {
		student := Student{
			Name:    fmt.Sprintf("student%d", i),
			Yuwen:   rand.Float64() * 100,
			Math:    rand.Float64() * 100,
			English: rand.Float64() * 100,
		}
		AvgStudent := (student.Yuwen + student.Math + student.English) / 3

		if AvgStudent < 60 {
			s1 = append(s1, student.Name)
		}

		fmt.Printf("%s 的平均分为: %.2f\n", student.Name, AvgStudent)

		SumStudent = SumStudent + AvgStudent*3

	}
	fmt.Printf("整个班三门平均分为: %.2f\n", SumStudent/float64(StudentNumber)/3)
	fmt.Printf("平均分低于 60 的同学共 %d 位,分别是 %s", len(s1), s1)
}

// 函数实现功能，尽量将结果用return返回
