package main

/*
创建一个student结构体，包含姓名和语数外三门课的成绩。
用一个slice容纳一个班的同学
求每位同学的平均分和整个班三门课的平均分，全班同学平均分低于60的有几位
*/

type Student struct {
	Name string
	Chinese float32
	Math float32
	English float32
}

func (s Student)CalMeanScore() float32 {
	sum:=s.Chinese + s.Math + s.English
	return sum / 3
}

type Class struct {
	//students []Student
	students []*Student
}

func (c Class)CalMeanScore() (float32,float32,float32) {
	var sumOfChinese,sumOfMath,sumOfEnglish float32 =0.,0.,0.
	for _, student := range c.students {
		sumOfChinese=student.Chinese
		sumOfMath=student.Math
		sumOfEnglish=student.English
	}
	cnt:=float32(len(c.students))
	if cnt==0 {
		return 0.,0.,0.
	}
	return sumOfChinese/cnt,sumOfMath/cnt,sumOfEnglish/cnt
}

func (c Class) buJiGe() int {
	cnt:=0
	for _, student := range c.students {
		if student.CalMeanScore() < 60 {
			cnt ++
		}
	}
	return cnt
}


//最后给个main函数调用一下吧