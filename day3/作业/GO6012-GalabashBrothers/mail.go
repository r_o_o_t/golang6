package main
//
import (
	"fmt"
	"math/rand"
	"time"
)

//矩阵生成函数, 需要传入行，列，及rand种子，返回一个二维切片
func matrix(x, y int, seed int64)  [][]int{
	rand.Seed(seed)
	matrix := make([][]int, x)
	for i:=0; i<x; i++ {
		matrix[i] = make([]int, y)
		for j := 0; j < y; j++ {
			matrix[i][j] = rand.Intn(128)
		}
	}
	return matrix
}

// 实现两个矩阵相加，return一个新的矩阵
func matrixSum(x, y [][]int) [][]int {
	if len(x) != len(y) {
		fmt.Println("x and y are not equal.")
	}
	matrixSum := make([][]int, len(x))
	for i := 0; i < len(x); i++ {
		if len(x[i]) == len(y[i]) {
			matrixSum[i] = make([]int, len(x[i]))
			for j := 0; j < len(x[i]); j++ {
				matrixSum[i][j] = x[i][j] + y[i][j]
			}
		} else {
			fmt.Printf("x[%d] and y[%d] are not equal.\n", i, i)
		}
	}
	return matrixSum
}


func seasonSwitch(i int) string{
	var season string
	switch i {
	case 3,4,5:
		season = "春季"
	case 6,7,8:
		season = "夏季"
	case 9,10,11:
		season = "秋季"
	case 12,1,2:
		season = "冬季"
	default:
		season = "请输入1-12整数"
	}
	return season
}

func seasonIf(i int) string{
	var season string
	if 3 <= i && i <=5 {
		season = "春季"
	}else if i>=6 && i<=8 {
		season = "夏季"
	}else if i>=9 && i<=11 {
		season = "秋季"
	}else if i==12 || i ==1 || i == 2 {
		season = "冬季"
	}else {
		season = "请输入1-12整数"
	}
	return season
}

// Student 学生结构体
type Student struct {
	Name string
	Math, English, Chinese int
}

// Class 班级结构体
type Class []*Student

// Avg 求平均分
func (s Student) Avg() int{
	return (s.Math + s.Chinese + s.English) / 3
}

// Avg 求班级总学生的课程平均分
func (c Class) Avg(course string) int {
	var sum int
	switch course {
	case "Math":
		for _, student := range c {
			//fmt.Printf("sum: %v student.Math: %v\n", sum, student.Math)
			sum += student.Math
		}
	case "English":
		for _, student := range c {
			sum += student.English
		}
	case "Chinese":
		for _, student := range c {
			sum += student.Chinese
		}
	}

	return sum / len(c)
}

// LessThan60 班级中平局分小于60的学生，返回map
func (c Class) LessThan60() map[string]int {
	S := make(map[string]int, len(c))
	for _, student := range c {
		if student.Avg() < 60 {
			S[student.Name] = student.Avg()
		}
	}
	return S
}

// 学生构造方法
func newStudent(Name string, Math, English, Chinese int) *Student {
	return &Student{
		Name: Name,
		Math: Math,
		English: English,
		Chinese: Chinese,
	}
}


func main()  {
	// 1.随机初始化两个8*5的矩阵，求两个矩阵的和（逐元素相加）
	matrix1 := matrix(8,5, 33)
	for _,v := range matrix1 {
		fmt.Printf("%p: %v\n", v,v )
	}
	fmt.Println("--------------------")
	matrix2 := matrix(8,5,time.Now().Unix())
	for _, v := range matrix2 {
		fmt.Printf("%p: %v\n", v,v )
	}
	fmt.Println("--------------------")
	// 两个矩阵相加的和
	for _, v := range matrixSum(matrix1, matrix2) {
		fmt.Printf("%p: %v\n", v,v )
	}

	// 2.给定月份，判断属于哪个季节。分别用if和switch实现。3-5月为春季，6-8月为夏季，9-11月为秋季，12-2月为冬季
	for i := -3; i < 14 ; i++ {
		fmt.Printf("Switch %d月是: %v\n", i, seasonSwitch(i))
		fmt.Printf("If %d月是: %v\n", i, seasonIf(i))
	}



	//3.创建一个student结构体，包含姓名和语数外三门课的成绩。用一个slice容纳一个班的同学，求每位同学的平均分和整个班三门课的平均分，全班同学平均分低于60的有几位
	var OneClass Class
	OneClass = append(OneClass, newStudent("张三", 80, 39, 2))
	OneClass = append(OneClass, newStudent("李四", 8, 36, 66))
	OneClass = append(OneClass, newStudent("王五", 26, 88, 99))
	OneClass = append(OneClass, newStudent("赵六", 22, 22, 66))
	OneClass = append(OneClass, newStudent("孙七", 55, 76, 47))

	// 打印学生数据，用于校验
	for i, student := range OneClass {
		fmt.Printf("第 %d 位学生情况：%#v\n", i, student)
	}

	// 打印所有学生的平均成绩
	for _, student := range OneClass {
		fmt.Printf("%v : %v\n",student.Name, student.Avg())
	}

	// 打印班级中语数外科目的平均成绩
	fmt.Printf("1班语文: %v\n", OneClass.Avg("Chinese"))
	fmt.Printf("1班数学: %v\n", OneClass.Avg("Math"))
	fmt.Printf("1班英语: %v\n", OneClass.Avg("English"))
	// 打印三门课的平均成绩，应该是总成绩/30

	// 打印班级中平均分小于60的同学 及分数
	for name, avg := range OneClass.LessThan60() {
		fmt.Printf("%v : %d\n", name,avg)
	}
}

// 输出不及格人数，不是每个人的信息