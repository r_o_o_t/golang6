package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

type student struct {
	name                             string
	chinease, math, english, average int
}

const n = 50 //一个班级50个学生
func main() {
	rand.Seed(time.Now().UnixNano())
	class := make([]student, n)
	for i := 0; i < n; i++ {
		class[i] = student{
			name:     "name" + strconv.Itoa(i), // 学生名字为name0,name1,name2...
			chinease: rand.Intn(100),
			math:     rand.Intn(100),
			english:  rand.Intn(100)}
		class[i].average = (class[i].chinease + class[i].math + class[i].english) / 3
	}
	// 输出结果
	fmt.Println("所有学生成绩如下：")
	for _, st := range class {
		fmt.Printf("%+v\n", st)
	}
	fmt.Println("全班学生的平均成绩如下：")
	var aveCh, aveMa, aveEng, fail int
	for _, st := range class {
		aveCh += st.average
		aveMa += st.math
		aveEng += st.english
		if st.average < 60 {
			fail++
		}
	}
	fmt.Printf("语文：%v\n数学：%v\n英语：%v\n", aveCh/n, aveMa/n, aveEng/n)
	fmt.Printf("不及格的学生有：%v名",fail)
}
