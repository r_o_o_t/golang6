package main

import "fmt"

func main() {
	var month int
	fmt.Printf("Please enter a month:(1~12) ")
	fmt.Scanf("%d", &month)
	// if else 
	if month < 1 || month > 12{
		fmt.Println("Not a correct month")
	} else if month <= 3 {
		fmt.Println("Spring")
	} else if month <= 6 {
		fmt.Println("Summer")
	} else if month <= 9 {
		fmt.Println("Autumn")
	} else {
		fmt.Println("Winter is coming")
	}
	// switch
	switch  {
	case month >=1 && month <= 3:
		fmt.Println("Spring")
	case month >=4 && month <= 6:
		fmt.Println("Summer")
	case month >=7 && month <= 9:
		fmt.Println("Spring")
	case month >=10 && month <=12 :
		fmt.Println("Winter is coming")
	default:
		fmt.Println("Not a correct month")
	}
}
