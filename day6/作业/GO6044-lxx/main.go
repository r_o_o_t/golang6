package main

import (
	"fmt"
	"time"
)

func timeParse(layout, s string) (time.Time, error) {
	loc, _ := time.LoadLocation("Asia/Shanghai")
	t, err := time.ParseInLocation(layout, s, loc)
	if err != nil {
		return time.Time{}, err
	}
	return t, nil
}

func timeFormatStr(sfmt string, t time.Time) string {
	s := t.Format(sfmt)
	return s
}

func getDayOfThisWeek(day time.Weekday) time.Time {
	t := time.Now()
	w := t.Weekday()

	switch {
	case w < day:
		offset := int(day - w)
		t = t.AddDate(0, 0, offset)
		return t
	case w > day:
		offset := int(day - w + 7)
		t = t.AddDate(0, 0, offset)
		return t
	default:
		return t
	}

}

func getClassDay(classday time.Weekday, n int) {
	// 获取最近的一个星期六
	layout := "2006-01-02"
	t := getDayOfThisWeek(classday)
	fmt.Printf("Class day is every %v of week. The follow 4 class day is below: \n", t.Weekday())

	oneWeek := time.Duration(7 * 24 * time.Hour)
	for i := 0; i < n; i++ {
		t = t.Add(oneWeek)
		fmt.Println(t.Format(layout))
	}
}

// func mergeTxt(dirEntry string) (string, error) {
// 	dir, err := ioutil.ReadDir(dirEntry)
// 	if err != nil {
// 		return "", err
// 	}
// 	bs := bytes.Buffer{}
// 	for _, f := range dir {
// 		fpath := filepath.Join(dir, f)
// 		if f.IsDir() {
// 			mergeTxt(fpath)
// 		} else if f.Mode().IsRegular() && strings.HasSuffix(fpath, ".txt") {
// 			// 直接写入到 file,而不是写到内存里
// 		}
// 	}
// }

// // compress/zlib
// // archive/zip
// func zipFile(infile string, outfile string) {
// 	f, err := os.Open(infile)
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// 	defer f.Close()
// 	// if
// 	fout, err := os.Open(outfile)
// 	writer := zlib.NewWriter(fout)
// 	writer.Write([]byte)
// 	writer.Flush()
// 	writer.Close()
// }

func main() {
	//////////////////////////
	// 1. 把字符串1998-10-01 08:10:00解析成time.Time，再格式化成字符串199810010810

	// layout := "2006-01-02 15:03:04"
	// t, _ := timeParse(layout, "1998-10-01 08:10:00")
	// fmt.Println(t)

	// layout2 := "20060102150304"
	// ts := timeFormatStr(layout2, t)
	// fmt.Println(ts)

	//////////////////////////
	// 2. 我们是每周六上课，输出我们未来4次课的上课日期（不考虑法定假日）
	// getClassDay(time.Saturday, 4)
	// getClassDay(time.Monday, 4)
	// getClassDay(time.Tuesday, 4)

	//////////////////////////////////////
	// 3. 把一个目录下的所有.txt文件合一个大的.txt文件，再对这个大文件进行压缩

}
